'use strict';

const noteAverage = require('./api/customer/extensions/note-average/graphql')
const extensions = [noteAverage]

/**
 * This module adds custom GraphQL query
 * @param strapi
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
module.exports = (strapi) => {
  const extensionService = strapi.plugin('graphql').service('extension')

  for (const extension of extensions) {
    extensionService.use(extension(strapi))
  }
}
