'use strict';

/**
 * This module defines the custom GraphQL query "customerNoteAvg" definition
 * @param strapi
 * @returns {function({nexus: *}): {typeDefs: string, resolvers: {Query: {customerNoteAvg: {resolve: function(*, *, *): Promise<{avg: number}>}}}, resolversConfig: {"Query.customerNoteAvg": {auth: {scope: [string]}}}}}
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
module.exports = (strapi) => ({nexus}) => ({
  typeDefs: `
    type CustomerNoteAvgResponse {
      avg: Float!
    }

    extend type Query {
      customerNoteAvg: CustomerNoteAvgResponse
    }
  `,
  resolvers: {
    Query: {
      customerNoteAvg: {
        /**
         * This function allows to resolve to the customers notes average service logic
         * @param parent
         * @param args
         * @param context
         * @returns {Promise<{avg: (number|number)}>}
         * @author Zied Khechin <ziedkhechin@ieee.org>
         */
        resolve: async (parent, args, context) => {
          const customers = await strapi.entityService.findMany('api::customer.customer'); // get all customers
          const count = customers.length; // count customers
          const noteSum = customers.reduce((a, b) => a + (b.note ?? 0), 0); // count notes sum

          return { // return the average of the notes
            avg: count > 0 ? noteSum / count : 0,
          };
        }
      }
    },
  },
  resolversConfig: {
    'Query.customerNoteAvg': {
      auth: {
        scope: ['api::customer.customer.findOne']
      }
    }
  }
})
