Production Ready (Backend)
=======================================

[![Build Status](https://api.travis-ci.org/python/mypy.svg?branch=master)](https://travis-ci.org/python/mypy)

## 🧾 Contents
* [About the project](#)
* [Directory tree](#)
* [Getting Started](#)
  * [Prerequisites](#)
  * [Installing](#)
  * [Running the project](#)
* [Authors](#)

## 💭 About the project
"Production Ready" is an evaluation project for the technical interview by ItSide Company.

## 🌳 Directory tree
```text
├───build
├───config
├───database
│   └───migrations
├───public
│   └───uploads
└───src
    ├───admin
    ├───api
    │   └───customer
    │       ├───content-types
    │       │   └───customer
    │       ├───controllers
    │       ├───extensions
    │       │   └───note-average
    │       ├───routes
    │       └───services
    └───extensions
```

## 🏁 Getting Started

#### 📋 Prerequisites
* You need [Git](https://git-scm.com/) and [Node.js](https://nodejs.org/) to install the project.

#### 📥 Installing
Node version must be 16 or higher.
```sh
> npm install
```
or
```sh
> yarn install
```

#### 🌠 Running the project
Execute this command to run the project:
- (with autoReload enabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-develop))
```
npm run develop
# or
yarn develop
```
- (with autoReload disabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-start))
```
npm run start
# or
yarn start
```

## 💼 Authors
* [**Zied Khechin**](https://www.linkedin.com/in/ziedkhechin/) - *Developer*

Project Repo: [https://gitlab.com/ziedkhechin/production-ready-backend](https://gitlab.com/ziedkhechin/production-ready-backend)
